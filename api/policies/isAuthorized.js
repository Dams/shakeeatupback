/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized with JSON web token
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */


module.exports = async function (req, res, next) {

  let token, verify;
  if (req.headers && req.headers.authorization) {
    token = req.headers.authorization;
  } else if (req.param('token')) {
    token = req.param('token');
    delete req.query.token;
  } else {
    return res.json({
      err: 'No Authorization header was found'
    });
  }
  try {
    verify = await sails.helpers.jwttokenverify({
      token: token,
      callback: {}
    });
    next();
  } catch (error) {
    console.log(await sails.helpers.getcurrentdate() + ": Le token n'est pas valide !");
    return res.json({
      err: 'Token is not valid'
    });
  }

}
