/**
 * isAtLeastClient
 *
 * @description :: Policy to check if user is Agent or CGP as Role
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */


module.exports = async function (req, res, next) {

  let token = await sails.helpers.parsejwt({
    token: req.headers.authorization,
  });
  let user = await Users.findOne({
    where: {
      id: token.token
    },
    select: ["role"]
  });
  if (user.role >= 0) {
    next();
  } else {
    return res.json({
      err: "N'a pas la permission pour acceder à ceci"
    });
  }
}
