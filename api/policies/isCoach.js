/**
 * isCoach
 *
 * @description :: Policy to check if user is CGP as Role
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */


module.exports = async function (req, res, next) {
  let token = await sails.helpers.parsejwt({
    token: req.headers.authorization,
  });
  console.log(token.token)
  let user = await Users.findOne({
    where: {
      id: token.token
    },
    select: ["role"]
  });
  if (user.role != 10) {
    return res.json({
      err: "N'a pas la permission pour acceder à ceci"
    });
  } else {
    next();
  }
}
