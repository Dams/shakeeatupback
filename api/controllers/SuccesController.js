/**
 * SuccesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    create: async function (req, res) {
        try {
          let succes = req.body
    
          let createdSucces = await Succes.create(succes).fetch().meta({
            skipRecordVerification: true
          });
    
          console.log(await sails.helpers.getcurrentdate() + ": Le succes " + createdSucces.id + " a ete cree")
          return res.json({
            resultat: createdSucces
          });
        } catch (error) {
          console.log(error)
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      updateSucces: async function (req, res) {
        try {
          let updatedSucces = await Succes.update({
            id: req.body.id
          }).set(req.body).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Le Succes " + req.body.id + " a ete mis a jour")
          return res.json({
            resultat: "done"
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      succesById: async function (req, res) {
        try {
          let id = req.param('id');
          let succes = await Succes.findOne({
            id: id
          }).meta({
            skipRecordVerification: true
          });
    
          console.log(await sails.helpers.getcurrentdate() + ": Le succes " + id + " a ete selectionne")
          return res.json({
            resultat: succes
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      allSucces: async function (req, res) {
        try {
          let allSucces = await Succes.find().meta({
            skipRecordVerification: true
          });
    
          return res.json({
            resultat: allSucces
          });
        } catch (error) {
    
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      },
};

