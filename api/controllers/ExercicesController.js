/**
 * ExercicesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    create: async function (req, res) {
        try {
          let exercice = req.body
    
          let createdExercices = await Exercices.create(exercice).fetch().meta({
            skipRecordVerification: true
          });
    
          console.log(await sails.helpers.getcurrentdate() + ": L'exercice " + createdExercices.id + " a ete crees")
          return res.json({
            resultat: createdExercices
          });
        } catch (error) {
          console.log(error)
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      updateExercice: async function (req, res) {
        try {
          let updatedExercices = await Exercices.update({
            id: req.body.id
          }).set(req.body).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": L'exercice " + req.body.id + " a ete mis a jour")
          return res.json({
            resultat: "done"
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      exerciceById: async function (req, res) {
        try {
          let id = req.param('id');
          let exercice = await Exercices.findOne({
            id: id
          }).meta({
            skipRecordVerification: true
          });
    
          console.log(await sails.helpers.getcurrentdate() + ": L'exercice " + id + " a ete selectionne")
          return res.json({
            resultat: exercice
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      allExercicesPerSeance: async function (req, res) {
        try {
          let id = req.param('id');
          let exercices = await Exercices.find({
            seances: id
          }).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Les exercices de la seance " + id + " ont ete selectionne")
          return res.json({
            resultat: exercices
          });
        } catch (error) {
          console.log(error)
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      },
      allExercicesPerCoach: async function (req, res) {
        try {
          let id = req.param('id');
          let exercice = await Exercices.find({
            user: id
          }).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Les exercices du coach " + id + " ont ete selectionne")
          return res.json({
            resultat: exercice
          });
        } catch (error) {
          console.log(error)
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      },
    
      allExercices: async function (req, res) {
        try {
          let id = req.param('id');
          let exercice = await Exercices.find().meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Tous les exercices ont ete selectionne")
          return res.json({
            resultat: exercice
          });
        } catch (error) {
          console.log(error)
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      }

};

