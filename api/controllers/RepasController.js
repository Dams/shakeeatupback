/**
 * RepasController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async function (req, res) {
    try {
      let repas = req.body

      let createdRepas = await Repas.create(repas).fetch().meta({
        skipRecordVerification: true
      });

      console.log(await sails.helpers.getcurrentdate() + ": Le repas " + createdRepas.id + " a ete crees")
      return res.json({
        resultat: createdRepas
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  updateRepas: async function (req, res) {
    try {
      let updatedRepas = await Repas.update({
        id: req.body.id
      }).set(req.body).meta({
        skipRecordVerification: true
      });
      console.log(await sails.helpers.getcurrentdate() + ": Le repas " + req.body.id + " a ete mis a jour")
      return res.json({
        resultat: "done"
      });
    } catch (error) {
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  repasById: async function (req, res) {
    try {
      let id = req.param('id');
      let repas = await Repas.findOne({
        id: id
      }).meta({
        skipRecordVerification: true
      });

      console.log(await sails.helpers.getcurrentdate() + ": Le repas " + id + " a ete selectionne")
      return res.json({
        resultat: repas
      });
    } catch (error) {
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  allRepasPerSeance: async function (req, res) {
    try {
      let id = req.param('id');
      let repas = await Repas.find({
        seances: id
      }).meta({
        skipRecordVerification: true
      });
      console.log(await sails.helpers.getcurrentdate() + ": Les repas de la seance " + id + " ont ete selectionne")
      return res.json({
        resultat: repas
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }
  },
  allRepasPerCoach: async function (req, res) {
    try {
      let id = req.param('id');
      let repas = await Repas.find({
        user: id
      }).meta({
        skipRecordVerification: true
      });
      console.log(await sails.helpers.getcurrentdate() + ": Les repas du coach " + id + " ont ete selectionne")
      return res.json({
        resultat: repas
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }
  },

  allRepas: async function (req, res) {
    try {
      let id = req.param('id');
      let repas = await Repas.find().meta({
        skipRecordVerification: true
      });
      console.log(await sails.helpers.getcurrentdate() + ": Tous les repas ont ete selectionne")
      return res.json({
        resultat: repas
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }
  }
};
