/**
 * ProgrammesTrainingController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: 'dyhcyolt3',
  api_key: '788992363594541',
  api_secret: 'dnS43XQk5S5zZ1NYQ0ygcP6KLq4'
});

module.exports = {
  
    create: async function (req, res) {
        try {
          let programme = req.body
    
          let createdProgramme = await ProgrammesTraining.create(programme).fetch().meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Le programme " + createdProgramme.id + " a ete crees")
          return res.json({
            resultat: createdProgramme
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      updateProgramme: async function (req, res) {
        try {
          let programme = await ProgrammesTraining.update({
            id: req.body.id
          }).set(req.body).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Le programme " + req.body.id + " a ete mis a jour")
          return res.json({
            resultat: "done"
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      programmeById: async function (req, res) {
        try {
          let id = req.param('id');
          let programme = await ProgrammesTraining.findOne({
            id: id
          }).meta({
            skipRecordVerification: true
          });
    
          console.log(await sails.helpers.getcurrentdate() + ": Le programme " + id + " a ete selectionne")
          return res.json({
            resultat: programme
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      allProgrammesPagination: async function (req, res) {
        try {
          let itemsPerPage = req.param('limit')
          let pageNum = req.param('page')
          let allProgrammes = await ProgrammesTraining.find({
              skip: itemsPerPage * (pageNum - 1),
              limit: itemsPerPage
            }
    
          ).meta({
            skipRecordVerification: true
          });
    
          return res.json({
            resultat: allProgrammes
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      },
      programmePerCoachPagination: async function (req, res) {
        try {
          let itemsPerPage = req.param('limit')
          let pageNum = req.param('page')
          let allProgrammes = await ProgrammesTraining.find({
              where: {
                user: req.param("id")
              },
              skip: itemsPerPage * (pageNum - 1),
              limit: itemsPerPage
            }
    
          ).meta({
            skipRecordVerification: true
          });
    
          return res.json({
            resultat: allProgrammes
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      },
      allProgrammes: async function (req, res) {
        try {
          let allProgrammes = await ProgrammesTraining.find().meta({
            skipRecordVerification: true
          });
    
          return res.json({
            resultat: allProgrammes
          });
        } catch (error) {
    
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      },
      programmePerCoach: async function (req, res) {
        try {
          let allProgrammes = await ProgrammesTraining.find({
            where: {
              user: req.param("id")
            },
          }).meta({
            skipRecordVerification: true
          });
    
          return res.json({
            resultat: allProgrammes
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
    
      },
      programmePerClient: async function (req, res) {
        try {
          let allProgrammes = await ProgrammesTraining.find({
            where: {
              clients: req.param("id")
            },
          }).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Les programmes du client " + req.param("id") + " ont ete selectionne")
          return res.json({
            resultat: allProgrammes
          });
        } catch (error) {
          console.log(error);
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
    
      },
      updateProgramme: async function (req, res) {
        try {
          let updatedProgramme = await ProgrammesTraining.update({
            id: req.body.id
          }).set(req.body).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Le programme " + req.body.id + " a ete mis a jour")
          return res.json({
            resultat: "done"
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
      },
    
      uploadImage: async function (req, res) {
        try {
          req.file('myImage').upload({
            // don't allow the total upload size to exceed ~10MB
            maxBytes: 10000000
          }, async function whenDone(err, uploadedFiles) {
            if (err) {
              return res.serverError(err);
            }
    
            // If no files were uploaded, respond with an error.
            if (uploadedFiles.length === 0) {
              return res.badRequest('No file was uploaded');
            }
    
            cloudinary.uploader.upload(uploadedFiles[0].fd, async function (result) {
              await ProgrammesTraining.update({
                id: req.param("id")
              }).set({
    
                image: result.secure_url
    
              }).meta({
                skipRecordVerification: true
              });
            });
    
    
    
    
            return res.json({
              resultat: "done"
            });
          });
        } catch (error) {
          console.log(error)
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      }
};

