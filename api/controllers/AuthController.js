/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var bcrypt = require('bcrypt');


function comparePassword(password, encryptedPassword, callback) {

  let myBool = false;
  bcrypt.compare(password, encryptedPassword, function (err, match) {
    if (err) cb(err);
    if (match) {
      myBool = true;
    } else {
      myBool = false;
    }
    callback(myBool);
  })

}

module.exports = {

  login: async function (req, res) {
    try {
      let userSelected = await Users.findOne({
        email: req.body.email
      });

      comparePassword(req.body.password, userSelected.password, async function (isSame) {
        
        if (isSame) {
          delete userSelected.password;
          console.log(await sails.helpers.getcurrentdate() + ": L'utilisateur " + userSelected.email + " vient de se connecter");
        } else {
          userSelected = "Mauvaise combinaison";
          console.log(await sails.helpers.getcurrentdate() + ": Erreur de connection pour l'utilisateur " + req.body.email);
        }

        return res.json({
          resultat: userSelected
        });
      });
    } catch (error) {

      return res.json({
        err: "Mauvaise combinaison"
      });
    }


  },
  logout: function (req, res) {
    return res.json({
      resultat: "logout",
    });
  }
};
