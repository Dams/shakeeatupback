/**
 * SuivisController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  suivisPerClient: async function (req, res) {
    try {
      let suivis = await Suivis.findOne({
        client: req.param("id")
      }).meta({
        skipRecordVerification: true
      });

      return res.json({
        resultat: suivis
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }
  }
};
