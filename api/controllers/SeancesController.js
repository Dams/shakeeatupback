/**
 * SeancesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async function (req, res) {
    try {
      let seance = req.body

      let createdSeance = await Seances.create(seance).fetch().meta({
        skipRecordVerification: true
      });

      console.log(await sails.helpers.getcurrentdate() + ": La seance " + createdSeance.id + " a ete crees")
      return res.json({
        resultat: createdSeance
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  updateSeance: async function (req, res) {
    try {
      let updatedSeance = await Seances.update({
        id: req.body.id
      }).set(req.body).meta({
        skipRecordVerification: true
      });
      console.log(await sails.helpers.getcurrentdate() + ": La seance " + req.body.id + " a ete mis a jour")
      return res.json({
        resultat: "done"
      });
    } catch (error) {
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  seanceById: async function (req, res) {
    try {
      let id = req.param('id');
      let seance = await Seances.findOne({
        id: id
      }).meta({
        skipRecordVerification: true
      });

      console.log(await sails.helpers.getcurrentdate() + ": La seance " + id + " a ete selectionne")
      return res.json({
        resultat: seance
      });
    } catch (error) {
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  allSeancesPerProgramme: async function (req, res) {
    try {
      let id = req.param('id');
      let seance = await Seances.find({
        programme: id
      }).meta({
        skipRecordVerification: true
      });
      console.log(seance)
      console.log(await sails.helpers.getcurrentdate() + ": Les seances du programme " + id + " ont ete selectionne")
      return res.json({
        resultat: seance
      });
    } catch (error) {
      return res.json({
        err: "Une erreur est survenue"
      })
    }
  },
  allSeancesPerCoach: async function (req, res) {
    try {
      let id = req.param('id');
      let seance = await Seances.find({
        user: id
      }).meta({
        skipRecordVerification: true
      });
      console.log(seance)
      console.log(await sails.helpers.getcurrentdate() + ": Les seances du coach " + id + " ont ete selectionne")
      return res.json({
        resultat: seance
      });
    } catch (error) {
      return res.json({
        err: "Une erreur est survenue"
      })
    }
  }
};

