/**
 * PalierNiveauController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    createPalier: async function (req, res) {
        try {
          let palierNiveau = req.body
    
          let createdPalierNiveau = await PalierNiveau.create(palierNiveau).fetch().meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Le palier " + createdPalierNiveau.id + " a ete crees")
          return res.json({
            resultat: createdProgramme
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      updatePalier: async function (req, res) {
        try {
          let palierNiveau = await PalierNiveau.update({
            id: req.body.id
          }).set(req.body).meta({
            skipRecordVerification: true
          });
          console.log(await sails.helpers.getcurrentdate() + ": Le palier " + req.body.id + " a ete mis a jour")
          return res.json({
            resultat: "done"
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },
      /* Récupère tous les niveau au dessus d'un certain palier d'exp 
         Prendre le premier enregistrement pour avoir le niveau de l'utilisateur */
      niveauByExp: async function (req, res) {
        try {
          let exp = req.param('exp');
          let palierNiveau = await PalierNiveau.find({
            exp: {'>=': exp}
          }).meta({
            skipRecordVerification: true
          });
    
          console.log(await sails.helpers.getcurrentdate() + ": Le programme " + exp + " a ete selectionne")
          return res.json({
            resultat: palierNiveau
          });
        } catch (error) {
          return res.json({
            err: "Une erreur est survenue"
          })
        }
    
      },

};

