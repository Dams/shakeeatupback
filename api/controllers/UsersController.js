/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var stripe = require('stripe')('sk_test_nTxW3780z10sAY4MiAe0C9Ne');
const sgMail = require('@sendgrid/mail');
//const jsPDF = require('jspdf');
sgMail.setApiKey("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");
//import * as jsPDF from 'jspdf'

module.exports = {
  create: async function (req, res) {
    try {
      let client = req.body

      let count = await Users.count({
        email: client.email
      }).meta({
        skipRecordVerification: true
      });

      if (count > 0) {
        return res.json({
          error: "L'utilisateur " + client.email + " existe déjà"
        })
      }
      let clientObject = {}
      if (client.role == 1) {
        clientObject = {
          password: client.password,
          email: client.email,
          nom: client.nom,
          prenom: client.prenom,
          role: 1,
          sex: client.sex,
          naissance: client.naissance,
          adresse: client.adresse,
          created: new Date().toISOString(),
          poids_depart: client.poids_depart,
          objectifs: client.objectifs
        }
      } else {
        clientObject = {
          password: client.password,
          email: client.email,
          nom: client.nom,
          prenom: client.prenom,
          role: 10,
          sex: client.sex,
          naissance: client.naissance,
          adresse: client.adresse,
          created: new Date().toISOString(),
        }
      }



      let createdUser = await Users.create(clientObject).fetch().meta({
        skipRecordVerification: true
      });
      delete createdUser.password;
      console.log(await sails.helpers.getcurrentdate() + ": L'utilisateur " + createdUser.id + " a ete crees")
      if (clientObject.role == 1) {
        let createdSuivis = await Suivis.create({
          "evolution": {
            "poids": [createdUser.poids_depart],
            "date": [new Date().toISOString()]
          },
          "client": createdUser.id
        }).fetch().meta({
          skipRecordVerification: true
        });
      }
      const msg = {
        to: clientObject.email,
        from: 'pichetevan@gmail.com.com',
        subject: 'Inscription Diet Coach',
        text: 'Merci de vous être inscrit sur notre plateforme de coaching',
        html: '<strong>Merci de vous être inscrit sur notre plateforme de coaching</strong>',
      };
      sgMail.send(msg);
      return res.json({
        resultat: 'done'
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  updateUser: async function (req, res) {
    try {
      let id = req.body.id

      /*delete req.body.id*/
      let updatedUser = await Users.update({
        /*email: req.body.email*/
        id: id
      }).set(req.body).meta({
        skipRecordVerification: true
      });
      console.log(await sails.helpers.getcurrentdate() + ": L'utilisateur " + id + " a ete mis a jour")
      if (req.body.role == 1) {
        let suivisClient = await Suivis.findOne({
          client: id
        }).meta({
          skipRecordVerification: true
        });

        suivisClient.evolution.poids.push(req.body.poids_depart);
        suivisClient.evolution.date.push(new Date().toISOString());
        let updateSuivis = await Suivis.update({
          id: suivisClient.id
        }).set(suivisClient).meta({
          skipRecordVerification: true
        });
      }


      return res.json({
        resultat: "done"
      });
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },
  userByUserId: async function (req, res) {
    try {
      let id = req.param('id');
      let user = await Users.findOne({
        id: id
      }).meta({
        skipRecordVerification: true
      });
      delete user.password;
      console.log(await sails.helpers.getcurrentdate() + ": L'utilisateur " + id + " a ete selectionne")
      return res.json({
        resultat: user
      });
    } catch (error) {
      return res.json({
        err: "Une erreur est survenue"
      })
    }

  },

  setSubscription: async function (req, res) {
    try {
      let id = req.param('id')
      let client = await Users.findOne({
        id: id
      }).meta({
        skipRecordVerification: true
      });
      let sub = await sails.helpers.subscription({
        client: client
      });

      client.customerKey = sub;
      await Users.update({
        id: id
      }).set(client).meta({
        skipRecordVerification: true
      });
      /*var doc = new jsPDF();

      doc.setFontSize(20);
      doc.text(75, 25, "Facture Diet Coaching");
      doc.text(150, 50, client.nom + " " + client.prenom);
      doc.text(150, 70, client.adresse[0]);
      doc.text(150, 90, client.adresse[2] + " " + client.adresse[1]);
      doc.text(10, 110, client.email);
      doc.text(150, 110, await sails.helpers.getcurrentdate());
      doc.text(10, 130, "Abonnement au programme de coaching");
      doc.text(10, 150, "Prix : 5 euros");
      doc.save('facture' + Math.random() + client.email.split('@') + '.pdf')
      console.log(doc); */
      const msg = {
        to: 'pichetevan@gmail.com', //client.email,
        from: 'pichetevan@gmail.com',
        subject: 'Facturation Diet Coaching',
        text: 'Merci de vous être abonnée à notre service de coaching en nutrition. ',
        html: '<strong>Merci de vous être abonnée à notre service de coaching en nutrition. Vous trouverez en pièce jointe votre facture.</strong>',
        attachments: [{
          content: 'JVBERi0xLjMKJbrfrOAKMyAwIG9iago8PC9UeXBlIC9QYWdlCi9QYXJlbnQgMSAwIFIKL1Jlc291cmNlcyAyIDAgUgovTWVkaWFCb3ggWzAgMCA1OTUuMjggODQxLjg5XQovQ29udGVudHMgNCAwIFIKPj4KZW5kb2JqCjQgMCBvYmoKPDwvTGVuZ3RoIDU2OT4+CnN0cmVhbQowLjU3IHcKMCBHCkJUCi9GMSAyMCBUZgoyMy4wMCBUTAowIGcKMjEyLjYwIDc3MS4wMiBUZAooRmFjdHVyZSBEaWV0IENvYWNoaW5nKSBUagpFVApCVAovRjEgMjAgVGYKMjMuMDAgVEwKMCBnCjQyNS4yMCA3MDAuMTYgVGQKKFBpY2hldCBFdmFuKSBUagpFVApCVAovRjEgMjAgVGYKMjMuMDAgVEwKMCBnCjQyNS4yMCA2NDMuNDYgVGQKKDI4IHJ1ZSBIZW5yaSBTaW1vbikgVGoKRVQKQlQKL0YxIDIwIFRmCjIzLjAwIFRMCjAgZwo0MjUuMjAgNTg2Ljc3IFRkCig3ODAwMCwgVmVyc2FpbGxlcykgVGoKRVQKQlQKL0YxIDIwIFRmCjIzLjAwIFRMCjAgZwoyOC4zNSA1MzAuMDggVGQKKHBpY2hldGV2YW5AZ21haWwuY29tKSBUagpFVApCVAovRjEgMjAgVGYKMjMuMDAgVEwKMCBnCjQyNS4yMCA1MzAuMDggVGQKKDA2LzA3LzIwMTgpIFRqCkVUCkJUCi9GMSAyMCBUZgoyMy4wMCBUTAowIGcKMjguMzUgNDczLjM5IFRkCihBYm9ubmVtZW50IGF1IHByb2dyYW1tZSBkZSBjb2FjaGluZykgVGoKRVQKQlQKL0YxIDIwIFRmCjIzLjAwIFRMCjAgZwoyOC4zNSA0MTYuNjkgVGQKKFByaXggOiA1IGV1cm9zKSBUagpFVAplbmRzdHJlYW0KZW5kb2JqCjEgMCBvYmoKPDwvVHlwZSAvUGFnZXMKL0tpZHMgWzMgMCBSIF0KL0NvdW50IDEKPj4KZW5kb2JqCjUgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL0Jhc2VGb250IC9IZWx2ZXRpY2EKL1N1YnR5cGUgL1R5cGUxCi9FbmNvZGluZyAvV2luQW5zaUVuY29kaW5nCi9GaXJzdENoYXIgMzIKL0xhc3RDaGFyIDI1NQo+PgplbmRvYmoKNiAwIG9iago8PAovVHlwZSAvRm9udAovQmFzZUZvbnQgL0hlbHZldGljYS1Cb2xkCi9TdWJ0eXBlIC9UeXBlMQovRW5jb2RpbmcgL1dpbkFuc2lFbmNvZGluZwovRmlyc3RDaGFyIDMyCi9MYXN0Q2hhciAyNTUKPj4KZW5kb2JqCjcgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL0Jhc2VGb250IC9IZWx2ZXRpY2EtT2JsaXF1ZQovU3VidHlwZSAvVHlwZTEKL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcKL0ZpcnN0Q2hhciAzMgovTGFzdENoYXIgMjU1Cj4+CmVuZG9iago4IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9CYXNlRm9udCAvSGVsdmV0aWNhLUJvbGRPYmxpcXVlCi9TdWJ0eXBlIC9UeXBlMQovRW5jb2RpbmcgL1dpbkFuc2lFbmNvZGluZwovRmlyc3RDaGFyIDMyCi9MYXN0Q2hhciAyNTUKPj4KZW5kb2JqCjkgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL0Jhc2VGb250IC9Db3VyaWVyCi9TdWJ0eXBlIC9UeXBlMQovRW5jb2RpbmcgL1dpbkFuc2lFbmNvZGluZwovRmlyc3RDaGFyIDMyCi9MYXN0Q2hhciAyNTUKPj4KZW5kb2JqCjEwIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9CYXNlRm9udCAvQ291cmllci1Cb2xkCi9TdWJ0eXBlIC9UeXBlMQovRW5jb2RpbmcgL1dpbkFuc2lFbmNvZGluZwovRmlyc3RDaGFyIDMyCi9MYXN0Q2hhciAyNTUKPj4KZW5kb2JqCjExIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9CYXNlRm9udCAvQ291cmllci1PYmxpcXVlCi9TdWJ0eXBlIC9UeXBlMQovRW5jb2RpbmcgL1dpbkFuc2lFbmNvZGluZwovRmlyc3RDaGFyIDMyCi9MYXN0Q2hhciAyNTUKPj4KZW5kb2JqCjEyIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9CYXNlRm9udCAvQ291cmllci1Cb2xkT2JsaXF1ZQovU3VidHlwZSAvVHlwZTEKL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcKL0ZpcnN0Q2hhciAzMgovTGFzdENoYXIgMjU1Cj4+CmVuZG9iagoxMyAwIG9iago8PAovVHlwZSAvRm9udAovQmFzZUZvbnQgL1RpbWVzLVJvbWFuCi9TdWJ0eXBlIC9UeXBlMQovRW5jb2RpbmcgL1dpbkFuc2lFbmNvZGluZwovRmlyc3RDaGFyIDMyCi9MYXN0Q2hhciAyNTUKPj4KZW5kb2JqCjE0IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9CYXNlRm9udCAvVGltZXMtQm9sZAovU3VidHlwZSAvVHlwZTEKL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcKL0ZpcnN0Q2hhciAzMgovTGFzdENoYXIgMjU1Cj4+CmVuZG9iagoxNSAwIG9iago8PAovVHlwZSAvRm9udAovQmFzZUZvbnQgL1RpbWVzLUl0YWxpYwovU3VidHlwZSAvVHlwZTEKL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcKL0ZpcnN0Q2hhciAzMgovTGFzdENoYXIgMjU1Cj4+CmVuZG9iagoxNiAwIG9iago8PAovVHlwZSAvRm9udAovQmFzZUZvbnQgL1RpbWVzLUJvbGRJdGFsaWMKL1N1YnR5cGUgL1R5cGUxCi9FbmNvZGluZyAvV2luQW5zaUVuY29kaW5nCi9GaXJzdENoYXIgMzIKL0xhc3RDaGFyIDI1NQo+PgplbmRvYmoKMTcgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL0Jhc2VGb250IC9aYXBmRGluZ2JhdHMKL1N1YnR5cGUgL1R5cGUxCi9GaXJzdENoYXIgMzIKL0xhc3RDaGFyIDI1NQo+PgplbmRvYmoKMTggMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL0Jhc2VGb250IC9TeW1ib2wKL1N1YnR5cGUgL1R5cGUxCi9GaXJzdENoYXIgMzIKL0xhc3RDaGFyIDI1NQo+PgplbmRvYmoKMiAwIG9iago8PAovUHJvY1NldCBbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KL0ZvbnQgPDwKL0YxIDUgMCBSCi9GMiA2IDAgUgovRjMgNyAwIFIKL0Y0IDggMCBSCi9GNSA5IDAgUgovRjYgMTAgMCBSCi9GNyAxMSAwIFIKL0Y4IDEyIDAgUgovRjkgMTMgMCBSCi9GMTAgMTQgMCBSCi9GMTEgMTUgMCBSCi9GMTIgMTYgMCBSCi9GMTMgMTcgMCBSCi9GMTQgMTggMCBSCj4+Ci9YT2JqZWN0IDw8Cj4+Cj4+CmVuZG9iagoxOSAwIG9iago8PAovUHJvZHVjZXIgKGpzUERGIDAuMC4wKQovQ3JlYXRpb25EYXRlIChEOjIwMTgwNzA2MTMzMzI5KzAyJzAwJykKPj4KZW5kb2JqCjIwIDAgb2JqCjw8Ci9UeXBlIC9DYXRhbG9nCi9QYWdlcyAxIDAgUgovT3BlbkFjdGlvbiBbMyAwIFIgL0ZpdEggbnVsbF0KL1BhZ2VMYXlvdXQgL09uZUNvbHVtbgo+PgplbmRvYmoKeHJlZgowIDIxCjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDc0MiAwMDAwMCBuIAowMDAwMDAyNTU5IDAwMDAwIG4gCjAwMDAwMDAwMTUgMDAwMDAgbiAKMDAwMDAwMDEyNCAwMDAwMCBuIAowMDAwMDAwNzk5IDAwMDAwIG4gCjAwMDAwMDA5MjQgMDAwMDAgbiAKMDAwMDAwMTA1NCAwMDAwMCBuIAowMDAwMDAxMTg3IDAwMDAwIG4gCjAwMDAwMDEzMjQgMDAwMDAgbiAKMDAwMDAwMTQ0NyAwMDAwMCBuIAowMDAwMDAxNTc2IDAwMDAwIG4gCjAwMDAwMDE3MDggMDAwMDAgbiAKMDAwMDAwMTg0NCAwMDAwMCBuIAowMDAwMDAxOTcyIDAwMDAwIG4gCjAwMDAwMDIwOTkgMDAwMDAgbiAKMDAwMDAwMjIyOCAwMDAwMCBuIAowMDAwMDAyMzYxIDAwMDAwIG4gCjAwMDAwMDI0NjMgMDAwMDAgbiAKMDAwMDAwMjgwNyAwMDAwMCBuIAowMDAwMDAyODkzIDAwMDAwIG4gCnRyYWlsZXIKPDwKL1NpemUgMjEKL1Jvb3QgMjAgMCBSCi9JbmZvIDE5IDAgUgovSUQgWyA8RTBERjFCNzA2OTAyQUU0MDEzRjE3NzRGN0VGMEFCMzY+IDxFMERGMUI3MDY5MDJBRTQwMTNGMTc3NEY3RUYwQUIzNj4gXQo+PgpzdGFydHhyZWYKMjk5NwolJUVPRg==',
          filename: 'facturepichetevan07062018.pdf',
          type: 'plain/text',
          disposition: 'attachment',
          content_id: 'mytext'
        }, ],
        //files: "C:/Users/Piche/Desktop/HITEMA/BackDietCoaching/.tmp/uploads/facturepichetevan07062018.pdf"
      };
      sgMail.send(msg);
      return res.json({
        resultat: client
      })
    } catch (error) {
      console.log(error)
      return res.json({
        err: "Une erreur est survenue"
      })
    }
  }
};
