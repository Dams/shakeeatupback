/**
 * Succes.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nom: {
      type: 'string',
      required: true
    },
    infos: {
      type: 'json',
      required: true
    },
    img: {
      type: 'json',
      defaultsTo: []
    },

    isunlocked: {
      type: 'string',
      required: true
    },
    
  },
  datastore: 'mongodb',
  schema: true
};

