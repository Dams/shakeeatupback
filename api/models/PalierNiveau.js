/**
 * PalierNiveau.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    niv: {
      type: 'Number',
      required: true
    },
    exp: {
      type: 'Number',
      required: true
    },
  },
  datastore: 'mongodb',
  schema: true
};

