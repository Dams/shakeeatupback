/**
 * Suivis.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    client: {
      type: 'string',
      required: true
    },
    commentaire: {
      type: 'string',
      required: false
    },
    programme: {
      type: 'string',
      required: false
    },
    evolution: {
      type: 'json',
      required: true
    },

  },
  datastore: 'mongodb',
  schema: true
};
