/**
 * SeancesTraining.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    etat: {
      type: 'number',
      required: true
    },
    programme: {
      type: 'json',
      defaultsTo: []
    },
    user: {
      type: 'string',
      required: true
    },
    nom: {
      type: 'string',
      required: true
    }
  },
  datastore: 'mongodb',
  schema: true

};

