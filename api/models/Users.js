/**
 * Users.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
var bcrypt = require('bcrypt');

module.exports = {

  attributes: {

    password: {
      type: 'string',
      required: true,
    },
    email: {
      type: 'string',
      required: true,
    },
    role: {
      type: 'number',
      defaultsTo: 1
    },
    prenom: {
      type: 'string',
      required: true
    },
    nom: {
      type: 'string',
      required: true
    },
    sex: {
      type: 'string',
      required: false,
    },
    naissance: {
      type: 'string',
      required: true
    },
    adresse: {
      type: 'string',
      required: false,
    },
    created: {
      type: 'string',
      required: true
    },
    poids_depart: {
      type: 'Number',
      required: false
    },
    objectifs: {
      type: 'json',
      required: false,
    },
    experience: {
      type: 'Number',
      required: false,
    },
    customerKey: {
      type: 'string',
      defaultsTo: ""
    }

  },
  beforeCreate: function (values, next) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) return next(err);
      bcrypt.hash(values.password, salt, function (err, hash) {
        if (err) return next(err);
        values.password = hash;
        next();
      })
    })
  },
  customToJson: function (obj) {
    delete obj.encryptedPassword;
    return obj;
  },
  datastore: 'mongodb',
  schema: true
};
