/**
 * Seances.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    etat: {
      type: 'number',
      required: true
    },
    programme: {
      type: 'json',
      defaultsTo: []
    },
    user: {
      type: 'string',
      required: true
    },
    nom: {
      type: 'string',
      required: true
    },
    exp: {
      type: 'Number',
      required: true
    },
  },
  datastore: 'mongodb',
  schema: true
};
