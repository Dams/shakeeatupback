/**
 * ProgrammesTraining.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    user: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string',
      required: true
    },
    image: {
      type: 'string',
      required: false
    },
    nom: {
      type: 'string',
      required: true
    },

    duree: {
      type: 'number',
      required: true
    },

    clients: {
      type: 'json',
      defaultsTo: []
    },
    type: {
      type: 'number',
      required: true
    }

  },
  datastore: 'mongodb',
  schema: true

};

