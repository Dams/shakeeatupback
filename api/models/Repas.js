/**
 * Repas.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nom: {
      type: 'string',
      required: true
    },
    infos: {
      type: 'json',
      required: true
    },
    ingredients: {
      type: 'json',
      required: true
    },
    prepa: {
      type: 'json',
      required: true
    },
    seances: {
      type: 'json',
      defaultsTo: []
    },

    periode: {
      type: 'json',
      defaultsTo: []
    },
    user: {
      type: 'string',
      required: true
    },
  },
  datastore: 'mongodb',
  schema: true
};
