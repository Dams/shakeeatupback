/**
 * Exercices.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    nom: {
      type: 'string',
      required: true
    },
    infos: {
      type: 'json',
      required: true
    },
    img: {
      type: 'string',
      required: false
    },
    seances: {
      type: 'json',
      defaultsTo: []
    },
    groupemuscle: {
      type: 'json',
      defaultsTo: []
    },
    serie: {
      type: 'string',
      required: true
    },
    rep: {
      type: 'string',
      required: true
    },
    tpsrecup: {
      type: 'string',
      required: true
    },
    user: {
      type: 'string',
      required: true
    },
    exp: {
      type: 'Number',
      required: true
    },
  },
  datastore: 'mongodb',
  schema: true

};

