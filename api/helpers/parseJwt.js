/**
 * jwToken
 *
 * @description :: JSON Webtoken Helper for sails
 * @help        :: See https://github.com/auth0/node-jsonwebtoken & http://sailsjs.org/#!/documentation/concepts/Services
 */

var jwt = require('jsonwebtoken'),
  tokenSecret = "secret";



module.exports = {

  friendlyName: 'parseJwt',


  description: 'Check if the token in the cookies is a jwtValid ',


  inputs: {

    token: {
      type: {},
      description: "Token contenu dans les cookies qui est à verifier",
      required: true
    }

  },
  exits: {
    success: {

    }
  },


  fn: async function (inputs, exits) {
    /*let base64Url = inputs.token.token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    let ret = JSON.parse(Buffer.from(base64, "base64").toString());
    return exits.success(ret);*/
  }

};
