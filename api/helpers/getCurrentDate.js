/**
 * getCurrentDate
 *
 * @description :: Get the current Date
 * 
 */


module.exports = {

  friendlyName: 'getCurrentDate',


  description: 'Get the current Date ',


  inputs: {

  },

  exits: {
    success: {

    }
  },


  fn: async function (inputs, exits) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    today = dd + '/' + mm + '/' + yyyy;

    return exits.success(today);
  }

};
