/**
 * jwTokenVerify
 *
 * @description :: JSON Webtoken Helper for sails
 * @help        :: See https://github.com/auth0/node-jsonwebtoken & http://sailsjs.org/#!/documentation/concepts/Services
 */

var jwt = require('jsonwebtoken'),
  tokenSecret = "secret";

module.exports = {

  friendlyName: 'jwtTokenVerify',


  description: 'Verify the Token to access the api ',


  inputs: {

    token: {
      type: {},
      description: "Token pour l'accès à l'api",
      required: true
    },
    callback: {
      type: 'ref',
    }

  },

  exits: {
    success: {

    }
  },


  fn: async function (inputs, exits) {
    console.log(await sails.helpers.getcurrentdate() + ": token à verifier:", inputs.token.token);
    let verify = jwt.verify(inputs.token.token, tokenSecret, {}, inputs.callback);
    return exits.success(verify);
  }

};
