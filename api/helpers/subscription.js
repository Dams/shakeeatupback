/**
 * subscription
 *
 * @description :: Subscribe to our product
 * 
 */

var stripe = require('stripe')('sk_test_nTxW3780z10sAY4MiAe0C9Ne');

module.exports = {

  friendlyName: 'subscription',


  description: 'Subscribe to our product',


  inputs: {
    client: {
      type: {},
      description: "client",
      required: true
    },
  },

  exits: {
    success: {

    }
  },


  fn: async function (inputs, exits) {
    let customerKey = inputs.client.client.customerKey
    let sub = {}
    let customer = {}
    if (customerKey == "") {
      customer = await stripe.customers.create({
        email: inputs.client.client.email,
        source: 'tok_visa'
      });
      customerKey = customer.id
    }
    console.log(customerKey)
    sub = await stripe.subscriptions.create({
      customer: customerKey,
      items: [{
        plan: "plan_DA5HXjFcjUnnzc",
      }, ]
    })

    return exits.success(customerKey);
  }

};
