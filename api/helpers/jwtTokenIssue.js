/**
 * jwTokenIssue
 *
 * @description :: JSON Webtoken Helper for sails
 * @help        :: See https://github.com/auth0/node-jsonwebtoken & http://sailsjs.org/#!/documentation/concepts/Services
 */

var jwt = require('jsonwebtoken'),
  tokenSecret = "secret";



module.exports = {

  friendlyName: 'jwtTokenIssue',


  description: 'Create a Token for a User to Access the api ',


  inputs: {

    token: {
      type: {},
      description: "Token pour l'accès à l'api",
      required: true
    }

  },
  exits: {
    success: {

    }
  },


  fn: async function (inputs, exits) {
    console.log(inputs.token)
    let token = jwt.sign(inputs.token, tokenSecret, {
      algorithm: 'HS256',
      expiresIn: 606024, // 7 days in secondes
    });
    return exits.success(token);
  }

};
