/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/

  '*': true, // Everything resctricted here with jwtToken

  'AuthController': {
    '*': true // no need of jwtToken or role
  },
  "UsersController": {
    'create': true, // no need of JwtToken
    'userByUserId': true,
    'updateUser': true,
    'setSubscription': true //['isClient']
  },
  "ProgrammesController": {
    'create': true, // no need of JwtToken,
    'updateProgramme': true,
    'programmeById': true,
    'allProgrammesPagination': true,
    'allProgrammes': true,
    'programmePerCoachPagination': true,
    'programmePerCoach': true,
    'programmePerClient': true
  },
  "SeancesController": {
    'create': true, // no need of JwtToken,
    'allSeancesPerProgramme': true,
    'allSeancesPerCoach': true,
    'seanceById': true,
    'updateSeance': true
  },
  "RepasController": {
    'create': true, // no need of JwtToken,
    'allRepasPerSeance': true,
    'allRepasPerCoach': true,
    'repasById': true,
    'allRepas': true,
    'updateRepas': true
  },
  "SuivisController": {
    'suivisPerClient': true,

  }
};
