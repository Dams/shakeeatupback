/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  '/*': function (req, res, next) {
    console.log(req.method, req.url);
    next();
  },

  /* GET USERS */
  'GET /': function (req, res) {
    return res.send('{"api":"welcome you"}');
  },
  'GET /users/:id': 'UsersController.userByUserId',
  'GET /users/subscription/:id': 'UsersController.setSubscription',

  /* POST USERS */
  'POST /users/update': 'UsersController.updateUser',
  'POST /users/create': 'UsersController.create',

  /* GET AUTH */
  'GET /auth/logout': 'AuthController.logout',

  /* POST AUTH */
  'POST /auth/login': 'AuthController.login',

  /* GET PROGRAMMES Diet */
  'GET /programmes/all': 'ProgrammesController.allProgrammes',
  'GET /programmes/all/:limit:/page': 'ProgrammesController.allProgrammesPagination',
  'GET /programmes/coach/:id': 'ProgrammesController.programmePerCoach',
  'GET /programmes/coach/:id/:limit:/page': 'ProgrammesController.programmePerCoachPagination',
  'GET /programmes/client/:id': 'ProgrammesController.programmePerClient',
  'GET /programmes/:id': 'ProgrammesController.programmeById',
  'GET /programmes/image/:id': 'ProgrammesController.downloadImage',

  /* POST PROGRAMMES Diet */
  'POST /programmes/create': 'ProgrammesController.create',
  'POST /programmes/update': 'ProgrammesController.updateProgramme',
  'POST /programmes/image/:id': 'ProgrammesController.uploadImage',

  /* GET SEANCES Diet */
  'GET /seances/programmes/:id': 'SeancesController.allSeancesPerProgramme',
  'GET /seances/coach/:id': 'SeancesController.allSeancesPerCoach',
  'GET /seances/:id': 'SeancesController.seanceById',

  /* POST SEANCES Diet */
  'POST /seances/create': 'SeancesController.create',
  'POST /seances/update': 'SeancesController.updateSeance',

  /* GET REPAS */
  'GET /repas/seance/:id': 'RepasController.allRepasPerSeance',
  'GET /repas/coach/:id': 'RepasController.allRepasPerCoach',
  'GET /repas/:id': 'RepasController.repasById',
  'GET /repas/all': 'RepasController.allRepas',

  /* POST REPAS */
  'POST /repas/create': 'RepasController.create',
  'POST /repas/update': 'RepasController.updateRepas',

  /* GET PROGRAMMESTRAINING */
  'GET /programmestraining/all': 'ProgrammesTrainingController.allProgrammes',
  'GET /programmestraining/all/:limit:/page': 'ProgrammesTrainingController.allProgrammesPagination',
  'GET /programmestraining/coach/:id': 'ProgrammesTrainingController.programmePerCoach',
  'GET /programmestraining/coach/:id/:limit:/page': 'ProgrammesTrainingController.programmePerCoachPagination',
  'GET /programmestraining/client/:id': 'ProgrammesTrainingController.programmePerClient',
  'GET /programmestraining/:id': 'ProgrammesTrainingController.programmeById',
  'GET /programmestraining/image/:id': 'ProgrammesTrainingController.downloadImage',

  /* POST PROGRAMMESTRAINING */
  'POST /programmestraining/create': 'ProgrammesTrainingController.create',
  'POST /programmestraining/update': 'ProgrammesTrainingController.updateProgramme',
  'POST /programmestraining/image/:id': 'ProgrammesTrainingController.uploadImage',

  /* GET SEANCESTRAINING*/
  'GET /seancestraining/programmes/:id': 'SeancesTrainingController.allSeancesPerProgramme',
  'GET /seancestraining/coach/:id': 'SeancesTrainingController.allSeancesPerCoach',
  'GET /seancestraining/:id': 'SeancesTrainingController.seanceById',

  /* POST SEANCESTRAINING */
  'POST /seancestraining/create': 'SeancesTrainingController.create',
  'POST /seancestraining/update': 'SeancesTrainingController.updateSeance',

  /* GET EXERCICES */
  'GET /exercice/seance/:id': 'ExercicesController.allExercicesPerSeance',
  'GET /exercice/coach/:id': 'ExercicesController.allExercicesPerCoach',
  'GET /exercice/:id': 'ExercicesController.exerciceById',
  'GET /exercice/all': 'ExercicesController.allExercices',

  /* POST EXERCICES */
  'POST /exercice/create': 'ExercicesController.create',
  'POST /exercice/update': 'ExercicesController.updateExercice',

  /* GET SUIVIS */
  'GET /suivis/:id': 'SuivisController.suivisPerClient',

  /* POST SUIVIS */

  /* GET SUCCES */

  /* POST SUCCES */

  /* GET CALENDRIER */

  /* POST CALENDRIER */





  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
